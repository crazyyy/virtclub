<!-- sidebar -->
<aside class="sidebar" role="complementary">

	<?php if ( is_active_sidebar('widgetarea1') ) : ?>
		<?php dynamic_sidebar( 'widgetarea1' ); ?>
	<?php else : ?>
 
    <div class="widget wiget-top">
        <h6>Топ модели в сети:</h6>
        <ul>
            <li>LisaKisa</li>
            <li>Валерия</li>
            <li>Алена</li>
            <li>Мия</li>
            <li>Stella</li>
            <li>Настя</li>
            <li>Лера</li>
            <li>Мия</li>
            <li>Stella</li>
            <li>Настя</li>
            <li>Лера</li>
        </ul>
    </div><!-- wiget-top -->
 
	<?php endif; ?>
</aside>
<!-- /sidebar -->
